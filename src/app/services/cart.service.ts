import { Injectable } from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';
import { CartState } from '../interfaces/cartState';
import { Product } from '../interfaces/product';
import {ProductDB} from "../interfaces/product-db";

@Injectable()
export class CartService {
  constructor() {}

  private cartState$ = new BehaviorSubject<CartState>({
    products: [],
    totalPrice: 0,
    isOpen: false
  });

  cartState = this.cartState$.asObservable();
  products: ProductDB[] = [];
  totalPrice = 0;
  isOpen = false;

  addProduct(recentProduct: any) {
    this.totalPrice = this.totalPrice + recentProduct.price;

    let exists = false;

    for (const product of this.products) {
      if (product.name === recentProduct.name) {
        product.quantity += 1;
        exists = true;
        break;
      }
    }

    if (!exists) {
      this.products.push({
        ...recentProduct,
        quantity: 1
      });
    }
    this.updateCartState();
  }

  deleteProduct(recentProduct: any, item: number) {
    this.totalPrice = this.totalPrice - recentProduct.price * recentProduct.quantity;
    this.products.splice(item, 1);
    this.updateCartState();
  }

  toggleCartList() {
    this.isOpen = !this.isOpen;
    this.updateCartState();
  }

  updateCartState() {
    this.cartState$.next(<CartState>{
      products: this.products,
      totalPrice: this.totalPrice,
      isOpen: this.isOpen
    });
  }

  clearCartState() {
    this.cartState$.next(<CartState>{
      products: [],
      totalPrice: 0,
      isOpen: false
    });
    this.products = [];
    this.totalPrice = 0;
    this.isOpen = false;
  }
}
