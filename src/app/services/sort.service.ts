import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class SortService {
  constructor() {}

  // bubble sort
  public bubbleSort(obj: Product[], sortBy: string) {
    let swapped = false;

    do {
      swapped = false;
      for (let i = 0; i < obj.length - 1; i++) {
        if (obj[i][sortBy] > obj[i + 1][sortBy]) {
          const temporaryPlace = obj[i];
          obj[i] = obj[i + 1];
          obj[i + 1] = temporaryPlace;
          swapped = true;
        }
      }
    } while (swapped);
  }
  // merge sort
  public mergeSort(arr: Product[], sortBy: string) {
    const sorted = [...this.mergeSortArr(arr, sortBy)];
    let index = 0;
    for (const sortedProduct of sorted) {
      arr[index] = sortedProduct;
      index++;
    }
  }

  private mergeSortArr(arr: Product[], sortBy: string) {
    if (arr.length < 2) {
      return arr;
    }
    const middle = Math.floor(arr.length / 2);
    const left = arr.slice(0, middle);
    const right = arr.slice(middle);
    return this.mergeArr(this.mergeSortArr(left, sortBy), this.mergeSortArr(right, sortBy), sortBy);
  }

  private mergeArr(left: Product[], right: Product[], sortBy: string) {
    const compareResult = [];
    let indexLeft = 0;
    let indexRight = 0;

    while (indexLeft < left.length && indexRight < right.length) {
      console.log(left[indexLeft]);
      if (left[indexLeft][sortBy] < right[indexRight][sortBy]) {
        compareResult.push(left[indexLeft]);
        indexLeft++;
      } else {
        compareResult.push(right[indexRight]);
        indexRight++;
      }
    }
    return compareResult.concat(left.slice(indexLeft)).concat(right.slice(indexRight));
  }
}
