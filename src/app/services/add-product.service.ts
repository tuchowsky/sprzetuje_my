import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AddProductService {

  private url = 'http://localhost:8080/api/productsDB/create';

  constructor(private http: HttpClient) { }

  addProduct(product) {
    return this.http.post(this.url, product);
  }
}

