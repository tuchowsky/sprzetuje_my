import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GetProductByNameService {

  public searchedProductName$ = new BehaviorSubject<any>(
    ''
  );



  private url = 'http://localhost:8080/api/productsDB';
  constructor(private http: HttpClient) { }

  getProductsByNameAndCategory(name, type) {

    if(type === '' || type === null || type === undefined) {
      type === null;
    }
    if(name === '' || name === null || type === undefined) {
      type === null;
    }

    this.searchedProductName$.next(name);

    // console.log(name, type);
    return this.http
      // .get(`${this.url}/getByName?name=${name}`);
    .get(`${this.url}/getByNameAndCategory?name=${name}&type=${type}`);
  }

  //
  // getProducts(filter: object) {
  //    console.log(filter);
  //   const params = new HttpParams().append('filter', JSON.stringify(filter));
  //   return this.http
  //     .get(this.url, {params});
  // }
}
