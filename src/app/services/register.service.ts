import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private url = 'http://localhost:8080/api/user';

  constructor(private http: HttpClient, private router: Router) { }

  registerUser(user) {
    return this.http.post(`${this.url}/register`, user);
  }

  getUsernameAvailability(username: string): Observable<boolean> {
    const params = new HttpParams().append('username', username);
    // console.log((params + " params"));
    return this.http.get<boolean>(`${this.url}/get-username-availability`, {params});
  }

}
