import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Subject} from "rxjs";
import { Router } from '@angular/router';
import {LoggedInUser} from "../interfaces/loggedInUser";
import {CartService} from "./cart.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userLoggedIn = new Subject<boolean>();
  userRoleAndUsername$ = new Subject<LoggedInUser>();

  defaultUser: LoggedInUser = {
    role: 'User',
    username: ''
  }

 // private loginUrl = 'http://localhost:8080/api/user/login';
  private loginUrl = 'http://localhost:8080/api/user/login';

  constructor(private http: HttpClient, private router: Router, private cartService: CartService) { }

  loginUser(user) {
    this.userLoggedIn.next(true);
    this.cartService.clearCartState();
    return this.http.post(this.loginUrl, user);
  }


  logOutUser(): boolean {
    localStorage.removeItem('token');
    this.cartService.clearCartState();
    this.userLoggedIn.next(false);
    this.checkIfUserIsLoggedIn();
    this.userRoleAndUsername$.next(
      {
        role: this.defaultUser.role,
        username: this.defaultUser.username
      }
    );
    return false;
  }

  checkIfUserIsLoggedIn(): boolean {
    if(localStorage.getItem('token')) {
     // console.log('user is logged in and decode');
      this.userLoggedIn.next(true);
      return true;

    } else {
      // console.log(this.userLoggedIn);
      this.userLoggedIn.next(false);
      this.router.navigate(['/login']);
      return false;
    }
  }

  getToken(): string {
    return localStorage.getItem('token');
  }


  decodeUserRole(): void {
   if(localStorage.getItem('token') === null) {
     this.userRoleAndUsername$.next(
       {
         role: this.defaultUser.role,
         username: this.defaultUser.username
       }
     );
     // console.log(localStorage.getItem('token'));
     return;
   }

    let jwt = localStorage.getItem('token');
    let jwtData = jwt.split('.')[1];
    let decodedJwtJsonData = window.atob(jwtData);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);
    // console.log(decodedJwtData.role + ' s', decodedJwtData.username + ' s' );
    this.userRoleAndUsername$.next(
      {
        role: decodedJwtData.role,
        username: decodedJwtData.username
      }
    );

    // console.log(decodedJwtData.role + ' user role from service', decodedJwtData.username + ' username from service' );
  }
}
