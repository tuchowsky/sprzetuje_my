import { Injectable } from '@angular/core';
import {ProductDB} from "../interfaces/product-db";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SingleProductService {
  singleProduct: ProductDB[] = [];
  singleProductId: string = '';


  private url = 'http://localhost:8080/api/productsDB/getProductById';

  constructor(private http: HttpClient) { }

  insertSingleProduct(product: ProductDB): void {
    this.singleProduct = [];
    this.singleProduct.push(product);

  }

  displaySingleProduct(): ProductDB[] {

    if(!this.singleProduct.length) {
      this.getSingleProductById(this.singleProductId);
    }
    return this.singleProduct;
  }
 // do poprawy
  setSingleProductId(id: string): void {
    this.singleProductId = id;
    // console.log(this.singleProductId);
  }
  getSingleProductById(id: string): any{
    const product = this.http.get<ProductDB>(`${this.url}?id=${this.singleProductId}`);
    product.subscribe(product => {
      this.singleProduct.push(product);
    });
  }

}
