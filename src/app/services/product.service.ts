import { Injectable } from '@angular/core';
import { ProductsService } from './products.service';
import { Product } from '../interfaces/product';

@Injectable()
export class ProductService {
  constructor(private productsService: ProductsService) {}

  getProduct(type: string): Product[] {
    return this.productsService.products.filter(product => product.type.toLowerCase() === type);
  }

  // postProduct(): Product[] {
  //
  // }
}
