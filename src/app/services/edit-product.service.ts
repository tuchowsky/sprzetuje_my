import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {CartState} from "../interfaces/cartState";
import {ProductDB} from "../interfaces/product-db";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EditProductService {

  private url = 'http://localhost:8080/api/productsDB/getProductById';

  productToEdit: ProductDB[] = [];

  constructor(private http: HttpClient) { }

  onSelectProductToEdit(product: ProductDB): void {
    this.productToEdit = [];
    this.productToEdit.push(product);
  }

  getSingleProductById(id: string): any {
    return this.http.get<ProductDB>(`${this.url}?id=${id}`);
  }
}
