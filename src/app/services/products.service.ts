import { Product } from '../interfaces/product';

export class ProductsService {
  products: Product[] = [
    {
      name: 'Amplifier Denon',
      color: 'Black',
      type: 'Amplifiers',
      price: 1300,
      photo: 'https://images.crutchfieldonline.com/ImageHandler/trim/620/378/products/2017/17/033/g033AVX1400-F.jpg'
    },
    {
      name: 'Denon PMA-2500NE',
      color: 'Gold',
      type: 'Amplifiers',
      price: 2000,
      photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_AUXUeL0Y_fMcrS31VFat43U6DkGKEsIWMT8QP1BHYlycz40M'
    },
    {
      name: 'Stam Audio SA-87 Studio Mic',
      color: 'Silver',
      type: 'Microphones',
      price: 2500,
      photo: 'https://d1aeri3ty3izns.cloudfront.net/media/38/386647/1200/preview.jpg'
    },
    {
      name: 'Pioneer DJM-900NXS',
      color: 'Black',
      type: 'DJ Mixers',
      price: 5050,
      photo: 'https://images-na.ssl-images-amazon.com/images/I/91EI21JHoHL._SX679_.jpg'
    },
    {
      name: 'Denon',
      color: 'Black',
      type: 'Amplifiers',
      price: 1100,
      photo: 'https://images.crutchfieldonline.com/ImageHandler/trim/620/378/products/2017/17/033/g033AVX1400-F.jpg'
    },
    {
      name: 'SA-87 Studio Mic',
      color: 'Silver',
      type: 'Microphones',

      price: 2000,
      photo: 'https://d1aeri3ty3izns.cloudfront.net/media/38/386647/1200/preview.jpg'
    },
    {
      name: 'DJM-900NXS',
      color: 'Black',
      type: 'DJ Mixers',
      price: 5000,
      photo: 'https://images-na.ssl-images-amazon.com/images/I/91EI21JHoHL._SX679_.jpg'
    }
  ];

  categories = [
    {
      type: 'Amplifiers',
      typeToRoute: 'amplifiers'
    },
    {
      type: 'Microphones',
      typeToRoute: 'microphones'
    },
    {
      type: 'DJ Equipment',
      typeToRoute: 'dj equipment'
    },
    {
      type: 'Speakers',
      typeToRoute: 'speakers'
    }
  ];
}
