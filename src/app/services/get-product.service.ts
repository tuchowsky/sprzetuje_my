import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GetProductService {

  private url = 'http://localhost:8080/api/productsDB/getAll';

  constructor(private http: HttpClient) { }

  getAllProducts() {
    return this.http.get(this.url);
  }

  getAllProductsFromCategory(category){
    return this.http.get(`${this.url}?type=${category}`);
  }
}
