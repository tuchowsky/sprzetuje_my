import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replacewhitespacetohypen'
})
export class ReplacewhitespacetohypenPipe implements PipeTransform {
  transform(value: string, args?: any): string {
    return value.replace(/\s/g, '-');
  }
}
