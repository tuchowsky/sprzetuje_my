import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CartService } from '../services/cart.service';
import { CartState } from '../interfaces/cartState';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {
  variable: boolean;

  cartState: CartState;

  private subscription: Subscription;
  constructor(private cartService: CartService) {
    this.variable = false;
  }

  ngOnInit() {
    this.subscription = this.cartService.cartState.subscribe((state: CartState) => {
      this.cartState = state;
    });
  }

  deleteItemFromCart(item): void {
    this.cartService.deleteProduct(this.cartState.products[item], item);
  }

  onHideCartList(): void {
    this.cartService.toggleCartList();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
