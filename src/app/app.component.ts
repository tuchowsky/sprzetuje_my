import {Component, OnInit} from '@angular/core';
import {LoginService} from "./services/login.service";
import {Subscription} from "rxjs";
import {LoggedInUser} from "./interfaces/loggedInUser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  private userRoleAndUsernameSubscription: Subscription;

  constructor(private loginService: LoginService) {

  }

  ngOnInit(): void {

    this.loginService.decodeUserRole();

  }
}


