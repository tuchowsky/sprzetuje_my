import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatIconModule,
  MatListModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatDialogModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';

import { FormComponent } from './form/form.component';
import { HeaderComponent } from './header/header.component';
import { BoardComponent } from './board/board.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { AddProductComponent } from './board/add-product/add-product.component';
import { ProductsComponent } from './dashboard/products/products.component';
import { ProductComponent } from './dashboard/products/product/product.component';
import { CartComponent } from './cart/cart.component';

import { RemovewhitespacesPipe } from './pipes/removewhitespaces.pipe';
import { ReplacewhitespacetohypenPipe } from './pipes/replacewhitespacetohypen.pipe';
import { FloatnumberPipe } from './pipes/floatnumber.pipe';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { NightModeService } from './shared/night-mode.service';
import { ProductService } from './services/product.service';
import { CartService } from './services/cart.service';
import { ProductCategoryService } from './shared/product-category.service';
import { TestComponent } from './test/test.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from "./guards/auth.guard";
import { RegisterComponent } from './auth/register/register.component';
import { AuthBoardComponent } from "./auth/auth-board.component";
import { OptionsComponent } from './tools/options/options.component';
import { AuthInterceptor } from "./interceptors/interceptor";
import { FooterComponent } from './footer/footer.component';
import { SingleProductComponent } from './dashboard/products/product/single-product/single-product.component';
import { PhotoModalComponent } from './modals/photo-modal/photo-modal.component';
import { EditProductComponent } from './board/edit-product/edit-product.component';
import { EditSingleProductComponent } from './board/edit-product/edit-single-product/edit-single-product.component';
import { DeleteModalComponent } from './modals/delete-modal/delete-modal.component';
import { EditComponent } from './board/edit-product/edit-single-product/edit/edit.component';
import {CanDeactivateGuardService} from "./guards/can-deactivate-guard.service";



@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    HeaderComponent,
    BoardComponent,
    DashboardComponent,
    AddProductComponent,
    RemovewhitespacesPipe,
    ReplacewhitespacetohypenPipe,
    ProductsComponent,
    ProductComponent,
    FloatnumberPipe,
    CartComponent,
    TestComponent,
    AuthBoardComponent,
    LoginComponent,
    RegisterComponent,
    OptionsComponent,
    FooterComponent,
    SingleProductComponent,
    PhotoModalComponent,
    EditProductComponent,
    EditSingleProductComponent,
    DeleteModalComponent,
    EditComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatDialogModule
  ],
  entryComponents: [
    PhotoModalComponent,
    DeleteModalComponent
  ],
  providers: [
    ProductCategoryService,
    NightModeService,
    ProductService,
    CartService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    CanDeactivateGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {}
