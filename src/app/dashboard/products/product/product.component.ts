import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../interfaces/product';
import { CartService } from '../../../services/cart.service';
import {ProductDB} from "../../../interfaces/product-db";
import {SingleProductService} from "../../../services/single-product.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  myVar = true;

  //@Input() singleProductFromCategory: Product[];

  @Input() singleProductDB: ProductDB;

  constructor(private cartService: CartService, private singleProductService: SingleProductService) {

  }

  addProductToCart(product: ProductDB): void {
    this.cartService.addProduct(product);
  }

  ngOnInit() {
   // console.log(this.singleProductDB);
  }

  getSingleProductDB(product: ProductDB) {
    this.singleProductService.insertSingleProduct(product);
  }
}
