import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductDB} from "../../../../interfaces/product-db";
import {SingleProductService} from "../../../../services/single-product.service";
import {CartService} from "../../../../services/cart.service";
import {MatDialog} from "@angular/material";
import {PhotoModalComponent} from "../../../../modals/photo-modal/photo-modal.component";

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit, OnDestroy {

  id: string;
  mainPhotoIndex = 0;

  private idSubscription: any;

  singleProduct: ProductDB[] = [];

  //

  constructor(private route: ActivatedRoute,
              private singleProductService: SingleProductService,
              private cartService: CartService,
              public dialog: MatDialog) {
    this.idSubscription = this.route.params.subscribe(params => {
      this.id = params.id;
    });
    this.singleProductService.setSingleProductId(this.id);
    this.singleProduct = this.singleProductService.displaySingleProduct();
  }

  ngOnInit() {


  }

  addProductToCart(product: ProductDB[]): void {
    console.log(product);
    this.cartService.addProduct(product);
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(PhotoModalComponent, {
      width: '99.99%',
      height: '100%',
      data: this.singleProduct
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    // });
  }

  onChangeMainPhoto(i) {
    this.mainPhotoIndex = i;
  }

  ngOnDestroy(): void {
    this.idSubscription.unsubscribe();
  }
}
