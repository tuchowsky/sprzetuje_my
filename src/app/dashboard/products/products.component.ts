import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router, Event, NavigationStart} from '@angular/router';
import { Product } from '../../interfaces/product';
import { ProductService } from '../../services/product.service';
import '../../typeExtensions/bubbleSort';
import '../../typeExtensions/mergeSort';
import {ProductDB} from "../../interfaces/product-db";
import {FormControl, FormGroup} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {GetProductByNameService} from "../../services/get-product-by-name.service";
import {GetProductService} from "../../services/get-product.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  providers: [ProductService]
})
export class ProductsComponent implements OnInit, OnDestroy {
  @Input() category: string;
  sortItemsBy = 'price';
  allProductsFromCategory: Product[];
  sortFieldHidden = true;
  findProductFieldHidden = true;
  searchResultHintHidden = true;

  searchForm: FormGroup;
  findProductInput = new FormControl('')
  inputValue = '';

  private searchedProductSubscription: Subscription;

  @Input() productsDB: ProductDB[];
  productsDBName: string[] = [];


  constructor(private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService,
              private getProductByNameService: GetProductByNameService) {


  }

  ngOnInit() {
    this.searchedProductSubscription = this.getProductByNameService.searchedProductName$.subscribe(state => {
      state = this.inputValue;
    });


    this.searchForm = new FormGroup({
      findProductInput: new FormControl(null),
      }
    );

    this.route.queryParams.subscribe((params) => {
      this.category = params.type;

      // console.log(params);

      if(this.category === undefined) {
        this.category = '';
      }

    });

     this.searchForm.get('findProductInput').valueChanges.pipe(
      debounceTime(1000)
    ).subscribe((value: string) => {

      this.inputValue = value;

      // console.log('before set products', this.inputValue, this.category);
      this.getProductByNameService.getProductsByNameAndCategory(this.inputValue, this.category).subscribe((res: ProductDB[]) => {

        this.productsDB = [];
        this.productsDB.push(...res);
        this.productsDBName = [];
        this.productsDB.map(product => {
          if(value === ''){
            this.productsDBName = [];
          } else {
            this.productsDBName.push(product.name);
          }
          //console.log(product.name);
        });
      }, err => {
        console.log('you get error on product search madafcuka');
      });
    })
  }

  onSort(): void {
    this.productsDB.mergeSort(x => x[this.sortItemsBy]);
  }

  onSortSubmit(): void {
    if(this.sortItemsBy === 'priceHL') {
      this.productsDB.sort((item1, item2) => (item1.price > item2.price) ? -1 : 1);

    } else  {
      console.log(this.onSort());
      this.onSort();
    }
  }

  onSortOptionSelected(event): void {
    this.sortItemsBy = event;
  }

  onShowSortFiled(): void {
    this.sortFieldHidden = !this.sortFieldHidden;
  }

  onShowFindProductField(): void {
    this.findProductFieldHidden = !this.findProductFieldHidden;
  }

  setInputValue(value) {
    this.searchForm.get('findProductInput').setValue(value);
    this.searchResultHintHidden = true;
    console.log(value);
  }

  ngOnDestroy(): void {
    this.searchedProductSubscription.unsubscribe();
  }

}
