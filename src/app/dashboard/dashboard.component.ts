import {Component, OnDestroy, OnInit} from '@angular/core';

import { ProductsService } from '../services/products.service';
import {HttpClient} from "@angular/common/http";
import {LoginService} from "../services/login.service";
import {GetProductService} from "../services/get-product.service";
import {ProductDB} from "../interfaces/product-db";
import {Router, Event, NavigationEnd, ActivatedRoute, ActivationStart, RoutesRecognized} from "@angular/router";
import {GetProductByNameService} from "../services/get-product-by-name.service";
import {error} from "@angular/compiler/src/util";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ProductsService]
})
export class DashboardComponent implements OnInit, OnDestroy {
  private sideNavOpen = false;
  public categories = [];
  private chosenCategory: string;
  private category = '';

  public allProducts: ProductDB[] = [];

  private searchedProduct = ''
  private searchedProductSubscription: Subscription;

  constructor(private productsService: ProductsService,
              private http: HttpClient,
              private loginService: LoginService,
              private getProductService: GetProductService,
              private router: Router,
              private route: ActivatedRoute,
              private getProductByNameService: GetProductByNameService) {

    this.allProducts = [];

    this.route.queryParams.subscribe((params) => {
      this.category = params.type;

      if(this.category === undefined) {
        this.category = '';
      }

      setTimeout(()=> {
        if(this.allProducts.length === 0){
          // console.log('wchodze')
          this.onShowCategoryProducts(this.category);
        } else  {
          return;
        }
        }, 1000)
      });

    this.router.events.subscribe((event: Event) => {
      if(event instanceof NavigationEnd && event.url === '/dashboard') {
        this.onShowCategoryProducts(this.category);
      }
    });
  }


  ngOnInit() {
    this.searchedProductSubscription = this.getProductByNameService.searchedProductName$.subscribe(state => {
      this.searchedProduct = state;
    });

    this.loginService.decodeUserRole();
    this.categories = this.productsService.categories;
  }

  toggleSideNav() {
    this.sideNavOpen = !this.sideNavOpen;
  }

  onShowCategoryProducts(category) {
    // console.log('before send dashboard', this.searchedProduct, category);
    this.getProductByNameService.getProductsByNameAndCategory(this.searchedProduct,  category).subscribe((res: ProductDB[]) => {
      this.allProducts = [];
      this.allProducts.push(...res)
    }), err => {
      console.log('you get error on dashboard get product madafcuka');
    };
    //this.chosenCategory = category;
  }
  //
  // onShowAllProducts() {
  //   this.getProductService.getAllProducts().subscribe((res: ProductDB[]) => {
  //     this.allProducts = [];
  //     this.allProducts.push(...res);
  //   }, err => {
  //     console.log('you get error on dashboard get product madafcuka');
  //   })
  //}

  ngOnDestroy(): void {
    this.searchedProductSubscription.unsubscribe();
  }
}
