import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductCategoryService} from "../../../../shared/product-category.service";
import {EditProductService} from "../../../../services/edit-product.service";
import {ProductDB} from "../../../../interfaces/product-db";
import {ActivatedRoute} from "@angular/router";
import {map} from "rxjs/operators";
import {__await} from "tslib";
import {interval} from "rxjs";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  productCategory: string[] = [];
  productToEdit: ProductDB[] = [];
  editProductForm: FormGroup;
  productID: string;
  productPhotos: string[] = [];
  photoInputHidden = true;
  photoIsEdited = false;
  formChanged = false;

  constructor(
    private productCategoryService: ProductCategoryService,
    private editProductService: EditProductService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.editProductForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'price': new FormControl(null, Validators.required),
      'type': new FormControl(null, Validators.required),
      'color': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required),
      'photos': new FormArray([])
    });
    this.productCategory = this.productCategoryService.getProductCategory();

    if (!this.editProductService.productToEdit.length) {
      this.route.params.subscribe(params => {
        this.productID = params.id;
        //
      });
      this.editProductService.getSingleProductById(this.productID);
      this.productToEdit = this.editProductService.productToEdit;
      this.editProductService.getSingleProductById(this.productID).subscribe(
        (response) => {
          this.productToEdit.push(response);
          this.onSetInputData();
        }
      );

    } else {
      this.productToEdit = this.editProductService.productToEdit;
      this.onSetInputData();
    }

  }

  onSetInputData() {
    this.productPhotos = [];
    for(let i = 0; i < this.productToEdit[0].photos.length; i++) {
      this.productPhotos.push(this.productToEdit[0].photos[i]);
    }
    this.editProductForm.setValue({
      'name': this.productToEdit[0].name,
      'price': this.productToEdit[0].price,
      'type': this.productToEdit[0].type,
      'color': this.productToEdit[0].color,
      'description': this.productToEdit[0].description,
      'photos': []
    })

    this.productPhotos.forEach((photo) => {
      const control = new FormControl(photo, Validators.required);
      (<FormArray>this.editProductForm.get('photos')).push(control);
    });
  }

  onUpdateProductInfo() {
    console.log('insomnia update works');
  }

  onEditPhoto() {
    this.photoInputHidden = !this.photoInputHidden;
    this.photoIsEdited = !this.photoIsEdited;
  }

  onUpdateProductPhoto(index) {

    let saved = 0;
    const photoArr = (<FormArray>this.editProductForm.get('photos'));
    const newPhotoURL = (<FormArray>this.editProductForm.get('photos')).at(index).value;
    this.productPhotos[index] = newPhotoURL;

    photoArr.controls.forEach((input, index) => {
      if(!this.productPhotos[index]) {
        console.log('weszlo')
        input.markAsDirty();

      } else {
        input.markAsPristine();
        saved++;
      }

    });

    if(saved === photoArr.controls.length) {
      this.photoInputHidden = !this.photoInputHidden;
      this.photoIsEdited = !this.photoIsEdited;
    }
    console.log(this.productPhotos);
    //(<FormArray>this.editProductForm.get('photos')).at(index).updateValueAndValidity();
  }

  onAddProductPhotoInput(index) {
    this.photoInputHidden = false;
    this.photoIsEdited = true;

    const photoArr = (<FormArray>this.editProductForm.get('photos'));
    if(!photoArr.at(photoArr.length - 1).value) {
      photoArr.at(photoArr.length - 1).markAsTouched();
    } else {
      const control = new FormControl(null, Validators.required);
      (<FormArray>this.editProductForm.get('photos')).push(control);

    }
  }

  onDeleteProductPhotoInput(index) {
    (<FormArray>this.editProductForm.get('photos')).removeAt(index);

    this.productPhotos.splice(index, 1);
    console.log(this.productPhotos);
  }

  clearPhotoValue(index){
    this.productPhotos[index] = undefined;
    console.log(this.productPhotos);
  }

  onFormChange(): void {

    this.editProductForm.valueChanges.subscribe(changes => {
      if(changes) {
        console.log('change Fired UP');
        this.formChanged = true;
      }
    });
  }

  hasChanges(): boolean {
    console.log(this.formChanged);
    return this.formChanged ? true : false;
  }

}
