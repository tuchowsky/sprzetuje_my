import {Component, Input, OnInit} from '@angular/core';
import {ProductDB} from "../../../interfaces/product-db";
import {MatDialog} from "@angular/material";
import {DeleteModalComponent} from "../../../modals/delete-modal/delete-modal.component";
import {EditProductService} from "../../../services/edit-product.service";

@Component({
  selector: 'app-edit-single-product',
  templateUrl: './edit-single-product.component.html',
  styleUrls: ['./edit-single-product.component.scss']
})
export class EditSingleProductComponent implements OnInit {

  itemControlsHidden = true;
  @Input() product: ProductDB;

  constructor(
    public dialog: MatDialog,
    private editProductService: EditProductService) { }

  ngOnInit() {
    // console.log(this.product);
  }

  onShowControls() {
    this.itemControlsHidden = !this.itemControlsHidden;
  }

  onDeleteProduct(): void {
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '50%',
      height: '60%',
      data: this.product,
      panelClass: 'delete-modal'
    });
  }

  onEditProduct(product: ProductDB) {
    this.editProductService.onSelectProductToEdit(product);
  }
}
