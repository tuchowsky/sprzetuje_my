import { Component, OnInit } from '@angular/core';
import {GetProductByNameService} from "../../services/get-product-by-name.service";
import {ProductDB} from "../../interfaces/product-db";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  productCategory = '';
  productname = '';

  products: ProductDB[] = [];

  constructor(private getProduct: GetProductByNameService) { }

  ngOnInit() {
    this.getProduct.getProductsByNameAndCategory(this.productCategory, this.productname).subscribe((state: ProductDB[]) => {
      this.products = [];
      this.products.push(...state);
    })
  }

}
