import {Component, OnInit} from '@angular/core';
import { NightModeService } from '../shared/night-mode.service';
import { Subscription } from 'rxjs';
import {LoginService} from "../services/login.service";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  nightMode: boolean;
  drawerClosed = true;
  private nightModeSubscription: Subscription;
  constructor(private nightModeService: NightModeService,
              private loginService: LoginService) {}

  ngOnInit() {
    this.nightModeSubscription = this.nightModeService.mode.subscribe((mode: boolean) => {
      this.nightMode = mode;
    });
  }

  toggleDrawer(): void {
    this.drawerClosed = !this.drawerClosed;
    console.log(this.drawerClosed);
  }
}
