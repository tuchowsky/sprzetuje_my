import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class NightModeService {
  mode = new BehaviorSubject<boolean>(true);

  changeMode(modeValue: boolean) {
    this.mode.next(modeValue);
  }
}
