import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  isOpen: boolean = false;
  optionListIsOpen = new BehaviorSubject<boolean>(this.isOpen);


  constructor() { }

  toggleOptionList(): void {
    this.isOpen = !this.isOpen;
    this.optionListIsOpen.next(this.isOpen);

  }
}
