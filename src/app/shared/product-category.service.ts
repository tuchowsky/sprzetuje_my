export class ProductCategoryService {
  private productCategory = [
    'Amplifiers',
    'Microphones',
    'DJ Equipment',
    'Speakers'
  ];

  constructor() {}

  getProductCategory() {
    return this.productCategory.slice();
  }
}
