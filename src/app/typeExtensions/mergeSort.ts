interface Array<T> {
  mergeSort<T>(this: T[], selector: (x: T) => any): Array<T>;
}

function mergeSort<T>(this: T[], selector: (x: T) => any) {
  const sorted = mergeSortArr(this, selector);
  let index = 0;
  for (const sortedProduct of sorted) {
    this[index] = sortedProduct;
    index++;
  }
  return this;
}

function mergeSortArr(tableToSort, sortBy) {
  if (tableToSort.length < 2) {
    return tableToSort;
  }

  const middle = Math.floor(tableToSort.length / 2);
  const left = tableToSort.slice(0, middle);
  const right = tableToSort.slice(middle);

  return mergeArr(mergeSortArr(left, sortBy), mergeSortArr(right, sortBy), sortBy);
}

function mergeArr(left: [], right: [], sortBy) {
  const compareResult = [];
  let indexLeft = 0;
  let indexRight = 0;

  while (indexLeft < left.length && indexRight < right.length) {
    if (sortBy(left[indexLeft]) < sortBy(right[indexRight])) {
      compareResult.push(left[indexLeft]);
      indexLeft++;
    } else {
      compareResult.push(right[indexRight]);
      indexRight++;
    }
  }
  return [...compareResult, ...left.slice(indexLeft), ...right.slice(indexRight)];
}

Array.prototype.mergeSort = mergeSort;
