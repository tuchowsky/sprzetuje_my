interface Array<T> {
  bubbleSort<T>(this: T[], selector: (x: T) => any): Array<T>;
}

function bubbleSort<T>(this: T[], selector: (x: T) => any) {
  let swapped = false;
  do {
    swapped = false;

    for (let i = 0; i < this.length - 1; i++) {
      if (selector(this[i]) > selector(this[i + 1])) {
        const temporaryPlace = this[i];
        this[i] = this[i + 1];
        this[i + 1] = temporaryPlace;
        swapped = true;
      }
    }
  } while (swapped);
  return this;
}

Array.prototype.bubbleSort = bubbleSort;
