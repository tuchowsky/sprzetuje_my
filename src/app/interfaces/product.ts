export interface Product {
  name: string;
  color: string;
  type: string;
  price: number;
  photo: string;
  quantity?: 1;
}
