export interface LoggedInUser {
  role: string,
  username: string
}
