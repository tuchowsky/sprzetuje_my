import { Product } from './product';
import {ProductDB} from "./product-db";

export interface CartState {
  totalPrice: number;
  products: ProductDB[];
  isOpen: boolean;
}
