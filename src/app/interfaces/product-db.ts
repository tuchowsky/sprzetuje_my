export interface ProductDB {
  id: string;
  name: string;
  color: string;
  type: string;
  price: number;
  photos: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  quantity?: 1;
}
