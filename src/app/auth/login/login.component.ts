import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from "../../services/login.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private loginService: LoginService, private router: Router) {

  }

  ngOnInit() {
    this.loginForm = new FormGroup({
        username: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.required)
      }
    )
  }

  onLogin() {
    if(this.loginForm.valid) {
      this.loginService.loginUser(this.loginForm.value).subscribe(
        (res: any) => {
          // console.log(res.token);
          localStorage.setItem('token', res.token);
          this.router.navigate(['/dashboard']);
          this.loginService.decodeUserRole();
        },
        err => console.log(err)
      );
    }
  }
}
