import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RegisterService} from "../../services/register.service";
import {LoginService} from "../../services/login.service";
import {Router} from "@angular/router";
import {debounceTime, filter} from "rxjs/operators";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  usernameDuplicate: boolean;


  constructor(private registerService: RegisterService,
              private loginService: LoginService,
              private router: Router) {

  }

  ngOnInit() {
    this.registerForm = new FormGroup({
        username: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.required),
        firstName: new FormControl(null, Validators.required),
        lastName: new FormControl(null, Validators.required)
      }
    );



    this.registerForm.get('username').valueChanges.pipe(
      debounceTime(500),
    ).subscribe(value => {
      console.log(value);
      if (value) {

        this.registerService.getUsernameAvailability(value)
          .subscribe(value => {
            //console.log(value + " input");
            this.usernameDuplicate = value;
          });
      }
      //console.log(this.registerForm.get('username'));
    });
    // console.log(this.registerForm
    //   .valueChanges.subscribe(value => {
    //   console.log(value.username);
    // }));
  }

  onRegister() {
    if(this.registerForm.valid) {
      this.registerService.registerUser(this.registerForm.value).subscribe(
        (res: any) => {
          //add statements if user email is found
          this.loginService.loginUser({
            username: res.username,
            password: this.registerForm.value.password
          }).subscribe(
            (res: any) => {
              localStorage.setItem('token', res.token);
              // console.log(localStorage.getItem('token'));
              this.loginService.decodeUserRole();
              this.router.navigate(['/dashboard']);
            },
            err => console.log(err)
          );
        },
        err => console.log(err)
      );
    }
  }
}
