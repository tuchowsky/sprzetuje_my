import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-auth-board',
  templateUrl: './auth-board.component.html',
  styleUrls: ['./auth-board.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
