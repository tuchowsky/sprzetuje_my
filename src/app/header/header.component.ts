import { Component, OnDestroy, OnInit } from '@angular/core';
import { NightModeService } from '../shared/night-mode.service';
import { Subscription } from 'rxjs';
import { CartService } from '../services/cart.service';
import { CartState } from '../interfaces/cartState';
import {LoginService} from "../services/login.service";
import {ToolsService} from "../shared/tools.service";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import {HttpClient} from "@angular/common/http";
import {LoggedInUser} from "../interfaces/loggedInUser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  nightMode: boolean;
  totalPrice: number;
  isOpen: boolean;
  userLoggedIn: boolean; //

  headerIsOpen = true;
  optionListIsOpen: boolean;
  recentUsername: string;

  private optionListSubscription: Subscription;
  private subscription: Subscription;
  private nightModeSubscription: Subscription;
  private loggedInSubscription: Subscription;

  private userRoleAndUsernameSubscription: Subscription;


  constructor(private nightModeService: NightModeService,
              private cartService: CartService,
              private loginService: LoginService,
              private toolsService: ToolsService,
              private http: HttpClient
              ) {
    this.totalPrice = 0;
    this.userRoleAndUsernameSubscription = this.loginService.userRoleAndUsername$.subscribe(
      (state: LoggedInUser) => {
        this.recentUsername = state.username;
        //console.log(this.recentUsername)
      }
    )
  }

  ngOnInit() {

    this.subscription = this.cartService.cartState.subscribe((state: CartState) => {
      this.totalPrice = state.totalPrice;
      this.isOpen = state.isOpen;
    });

    this.nightModeSubscription = this.nightModeService.mode.subscribe((mode: boolean) => {
      this.nightMode = mode;
    });

    this.optionListSubscription = this.toolsService.optionListIsOpen.subscribe((isOpen:boolean) => {
      this.optionListIsOpen = isOpen;
    })

    //
    this.userLoggedIn = this.loginService.checkIfUserIsLoggedIn();

    this.loggedInSubscription = this.loginService.userLoggedIn.subscribe(
      state => {
        // console.log(this.userLoggedIn + " user logged inn check")
        this.userLoggedIn = state;
      }
    )
  }

  onToggleHeader() {
    this.headerIsOpen = !this.headerIsOpen;
    console.log(this.headerIsOpen);
  }

  onShowCartList() {
    if(this.optionListIsOpen) {
      this.toolsService.toggleOptionList();
    }
    this.cartService.toggleCartList();
  }

  onShowOptionList() {
    // this.loginService.checkIfUserIsLoggedIn();
    if(!this.loginService.checkIfUserIsLoggedIn()) {
      return;
    } else {
      if(this.isOpen) {
        this.cartService.toggleCartList();
      }
      this.toolsService.toggleOptionList();
    }
  }
//
  onLogOut() {
    this.loginService.logOutUser();
    if(this.isOpen) {
      this.cartService.toggleCartList();
    }
    if(this.optionListIsOpen) {
      this.toolsService.toggleOptionList();
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.nightModeSubscription.unsubscribe();
    this.loggedInSubscription.unsubscribe();
    this.optionListSubscription.unsubscribe();
    this.userRoleAndUsernameSubscription.unsubscribe();
  }
}
