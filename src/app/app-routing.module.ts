import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { BoardComponent } from './board/board.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddProductComponent } from './board/add-product/add-product.component';

import { ProductsComponent } from './dashboard/products/products.component';
import {LoginComponent} from "./auth/login/login.component";
import {AuthGuard} from "./guards/auth.guard";
import {RegisterComponent} from "./auth/register/register.component";
import {SingleProductComponent} from "./dashboard/products/product/single-product/single-product.component";
import {EditProductComponent} from "./board/edit-product/edit-product.component";
import {EditComponent} from "./board/edit-product/edit-single-product/edit/edit.component";
import {CanDeactivateGuardService} from "./guards/can-deactivate-guard.service";

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'board',
    component: BoardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'add-product',
        component: AddProductComponent
      },
      {
        path: 'manage-product',
        component: EditProductComponent,
      },
      {
        path: 'manage-product/edit/:id',
        component: EditComponent,
        canDeactivate: [CanDeactivateGuardService]
      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'equipment',
        component: ProductsComponent,
      }
    ]
  },
  {
    path: 'product-info/:id',
    component: SingleProductComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
