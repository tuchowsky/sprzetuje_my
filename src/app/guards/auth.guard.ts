import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {LoginService} from "../services/login.service";


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
 constructor( private loginService: LoginService, private router: Router) {
 }

 canActivate():boolean {
    if (this.loginService.checkIfUserIsLoggedIn()) {
      console.log('auth true')
      return true;
    } else {
      this.router.navigate(['/login']);
      console.log('auth false')
      return false;
    }
 }
}
