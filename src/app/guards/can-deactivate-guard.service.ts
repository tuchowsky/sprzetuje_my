import { Injectable } from '@angular/core';
import {CanDeactivate, Router} from "@angular/router";
import { Location } from '@angular/common';
import { EditComponent } from "../board/edit-product/edit-single-product/edit/edit.component";




@Injectable()
export class CanDeactivateGuardService implements CanDeactivate<EditComponent>{

  constructor(private router: Router) {

  }

  canDeactivate(target: EditComponent) {
    console.log('weszlo to canDeactivate');
    console.log(target.hasChanges());
    if(target.hasChanges()) {
      return window.confirm('really want to change');
    }

    // this.router.navigate(['/board/manage-product']);
    return true;
  }
}

