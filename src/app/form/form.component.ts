import { Component, Injectable, OnInit } from '@angular/core';
import { ProductCategoryService } from '../shared/product-category.service';
import {FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {AddProductService} from "../services/add-product.service";

@Injectable()
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  productCategory = [];
  descriptionFieldHidden = true;
  addPhotoFieldHidden = true;
  addProductForm: FormGroup;
  addPhotosArrLength = 0;

  constructor(private productCategoryService: ProductCategoryService, private addProductService: AddProductService) {}

  ngOnInit() {
    this.addProductForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'color': new FormControl(null, Validators.required),
      'type': new FormControl(null, Validators.required),
      'price': new FormControl(null, Validators.required),
      'photos': new FormArray([]),
      'description': new FormControl(null),
      'checkbox': new FormControl(null, Validators.required)
    });
    this.productCategory = this.productCategoryService.getProductCategory();
    this.addPhotosArrLength = (<FormArray>this.addProductForm.get('photos')).length
  }

  onSubmitAddProduct() {

    // console.log(this.addProductForm.value);

    if(this.addProductForm.valid) {
      console.log(this.addProductForm.value);
      this.addProductService.addProduct(this.addProductForm.value).subscribe(res => {
        console.log(res);
      }, err => {
        console.log('this is error from add product madafcaka');
      })

    } else {
      alert('enter correct details fuCkEr');
    }

    this.clearValidation(this.addProductForm);
    this.addProductForm.reset();

  }

  onAddPhotoInput() {
    this.addPhotoFieldHidden = false;
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.addProductForm.get('photos')).push(control);
    this.addPhotosArrLength++;
  }

  onDeletePhotoInput(item) {
    (<FormArray>this.addProductForm.get('photos')).removeAt(item);
    this.addPhotosArrLength--;
    if(!this.addPhotosArrLength){
      this.addPhotoFieldHidden = !this.addPhotoFieldHidden;
    }
  }

  onShowDescriptionField() {
    this.descriptionFieldHidden = !this.descriptionFieldHidden;
  }

  onShowAddPhotoField() {
    if (!this.addPhotosArrLength) {
      this.onAddPhotoInput();
      this.addPhotoFieldHidden = !this.addPhotoFieldHidden;
    }
    this.addPhotoFieldHidden = !this.addPhotoFieldHidden;
  }

  clearValidation(form) {
    const inputs = ['name', 'color', 'type', 'price', 'photos', 'checkbox'];
    inputs.forEach(input => {
      form.get(input).clearValidators();
    })
  }

}
