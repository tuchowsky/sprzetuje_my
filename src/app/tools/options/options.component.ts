import {Component, OnDestroy, OnInit} from '@angular/core';
import {NightModeService} from "../../shared/night-mode.service";
import {Subscription} from "rxjs";
import {ToolsService} from "../../shared/tools.service";
import {LoginService} from "../../services/login.service";
import {LoggedInUser} from "../../interfaces/loggedInUser";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit, OnDestroy {

  optionListIsOpen: boolean;
  private optionListSubscription: Subscription;

  nightMode: boolean;
  private nightModeSubscription: Subscription;

  private userRoleAndUsernameSubscription: Subscription;

  userRoleAndUsername: LoggedInUser;

  constructor(private nightModeService: NightModeService,
              private toolsService: ToolsService,
              private loginService: LoginService) {

    this.userRoleAndUsernameSubscription = this.loginService.userRoleAndUsername$.subscribe(
      (state: LoggedInUser) => {
       // console.log(state.role + ' role');
        this.userRoleAndUsername = {
          role: state.role,
          username: state.username
        }
      }
    )
  }

  ngOnInit() {
    this.nightModeSubscription = this.nightModeService.mode.subscribe((mode: boolean) => {
      this.nightMode = mode;
    });

    this.optionListSubscription = this.toolsService.optionListIsOpen.subscribe((isOpen:boolean) => {
      this.optionListIsOpen = isOpen;
    });
  }

  onChangeNightMode() {
    this.nightModeService.changeMode(this.nightMode);
  }

  onToggleOptionList() {
    this.toolsService.toggleOptionList();
    console.log(this.optionListIsOpen + " options ");
  }

  ngOnDestroy(): void {
    this.nightModeSubscription.unsubscribe();
    this.userRoleAndUsernameSubscription.unsubscribe();
  }

}
