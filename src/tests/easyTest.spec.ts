import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestComponent } from '../app/test/test.component';
import { CartComponent } from '../app/cart/cart.component';
import { FloatnumberPipe } from '../app/pipes/floatnumber.pipe';
import { CartService } from '../app/services/cart.service';
import { HeaderComponent } from '../app/header/header.component';
import { NightModeService } from '../app/shared/night-mode.service';
import { MatIconModule, MatSlideToggleModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule } from '@angular/forms';
import { CartState } from '../app/interfaces/cartState';
import { Product } from '../app/interfaces/product';
import { ProductComponent } from '../app/dashboard/products/product/product.component';
import { MatCardModule } from '@angular/material/card';

describe('TestComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('value should be', () => {
    expect(component.variable).toBe(2);
  });
});

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  const proba = false;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent, FloatnumberPipe],
      providers: [NightModeService, CartService],
      imports: [FormsModule, MatSlideToggleModule, MatToolbarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('myVAr should be fale', () => {
    expect(component.myVar).toBeFalsy();
  });

  it('night mode should be true', () => {
    expect(component.nightMode).toBe(true);
  });

  it('start value of cart should be 0', () => {
    expect(component.totalPrice).toBe(0);
  });
});

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductComponent],
      providers: [CartService],
      imports: [MatCardModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    component.singleProductFromCategory = [
      {
        name: 'Onkyo',
        color: 'Red',
        type: 'Amplifiers',
        price: 2000,
        photo: 'image'
      }
    ];
    fixture.detectChanges();
  });

  it('should be true', () => {
    expect(component.myVar).toBeTruthy();
  });
});
